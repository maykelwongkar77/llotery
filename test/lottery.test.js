// const assert = require('assert');
// const ganache = require('ganache-cli');
// const Web3 = require('web3');
// const web3 = new Web3(ganache.provider());
// const {interface, bytecode} = require('../compile');


const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const provider = ganache.provider();
const web3 = new Web3(provider);
const {interface, bytecode} = require('../compile');

let lottery;
let accounts;

beforeEach(async () =>{
    accounts = await web3.eth.getAccounts();
    lottery = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({data:bytecode})
    .send({from : accounts[0], gas:'1000000'});
});

describe('Lottery Contract', () => {
    it('deploys a contract', () => {
        assert.ok('lottery.options.address');
    });

    // it('allows one account to enter', async () => {
    //     await lottery.methods.enter().send({
    //         from : accounts[0],
    //         value : web3.utils.toWei('0.02','ether')
    //     });
    //     const players = await lottery.methods.getPlayers().call({
    //         from : accounts[0]
    //     });

    //     assert.equal(accounts[0],players[0]);
    //     assert.equal(1, players.length);
    // });

    // it('allows multiple account to enter', async () => {
    //     await lottery.methods.enter().send({
    //         from : accounts[0],
    //         value : web3.utils.toWei('0.02','ether')
    //     });
    //     await lottery.methods.enter().send({
    //         from : accounts[1],
    //         value : web3.utils.toWei('0.02','ether')
    //     });
    //     await lottery.methods.enter().send({
    //         from : accounts[2],
    //         value : web3.utils.toWei('0.02','ether')
    //     });
    //     const players = await lottery.methods.getPlayers().call({
    //         from : accounts[0]
    //     });

    //     assert.equal(accounts[0],players[0]);
    //     assert.equal(accounts[1],players[1]);
    //     assert.equal(accounts[2],players[2]);
    //     assert.equal(3, players.length);
    // });
    // it('requires a minimum amount of ether to enter', async () => {
    //     //if it pass then this is error
    //     try{
    //         await lottery.methods.enter().send({
    //             from : accounts[0],
    //             value : web3.utils.toWei('0.1', 'ether')
    //         });
    //     }catch(err){
    //         assert(err);
    //         return
    //     }
    //     assert(false);
    // });
    // it('only manager can call pickWinner', async () => {
    //     //if it pass then this is error
    //     await lottery.methods.enter().send({
    //         from: accounts[0],//manager
    //         value: web3.utils.toWei('0.02', 'ether')
    //     });
    //     try{
    //         await lottery.methods.pickWinner().send({
    //             from : accounts[1]//testing when non-manager call the pickWinner
    //         });
    //     }catch(err){
    //         assert(err);
    //         return
    //     }
    //     assert(false);
    // });

    it('sends money to the winner and resets the players array', async() =>{
        try{
            await lottery.methods.enter().send({
                from : accounts[0],
                value : web3.utils.toWei('2', 'ether')
            });
        }catch(err){
            assert(err);
            return
        }
        const intialBalance = await web3.eth.getBalance(accounts[0]);

        try{
            await lottery.methods.pickWinner().send({
                from : accounts[0]
            });
        }catch(err){
            assert(err);
            return
        }
        //finalBalance = initialBalance - gas
        const finalBalance = await web3.eth.getBalance(accounts[0]);
        const difference =finalBalance - intialBalance;
        console.log(difference);
        assert(difference > web3.utils.toWei('1.8','ether'));
    });
});